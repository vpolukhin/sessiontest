class TestController < ApplicationController
  def set_initial_session
    session[:a] = 'initial_value'
    render json: { page: :set_initial_session }
  end

  def long_request
    sleep(2)
    render json: { page: :long_request }
  end

  def quick_request
    sleep(1)
    session[:a] = 'from quick_request'
    render json: { page: :quick_request }
  end

  def view_session
    render json: { session: session }
  end

  def page
    render :page
  end
end
