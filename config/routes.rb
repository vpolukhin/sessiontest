Rails.application.routes.draw do
  get 'page', to: 'test#page'

  get 'set_initial_session', to: 'test#set_initial_session'
  get 'long_request', to: 'test#long_request'
  get 'quick_request', to: 'test#quick_request'
  get 'view_session', to: 'test#view_session'

end
