# frozen_string_literal: true

Rails.application.config.session_store :redis_session_store, {
  key: 'sessiontest',

  redis: {
    ttl: 30.days,
    key_prefix: 'sessiontest:session:',
    url: 'redis://localhost:6379/0',
  }
}
